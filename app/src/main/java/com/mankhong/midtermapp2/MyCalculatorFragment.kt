package com.mankhong.midtermapp2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.mankhong.midtermapp2.databinding.FragmentMyCalculatorBinding
import com.mankhong.midtermapp2.databinding.FragmentPlusBinding


class MyCalculatorFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var _binding: FragmentMyCalculatorBinding? = null
    private  val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMyCalculatorBinding.inflate(inflater, container, false)
        return binding?.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.imagePlusButton?.setOnClickListener {
            val action = MyCalculatorFragmentDirections.actionMyCalculatorFragmentToPlusFragment()
            view.findNavController().navigate(action)
        }
        binding?.imageMinusButton?.setOnClickListener {
            val action = MyCalculatorFragmentDirections.actionMyCalculatorFragmentToMinusFragment()
            view.findNavController().navigate(action)
        }

    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {

    }
}